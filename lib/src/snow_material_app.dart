import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'widgets/flavor_banner/flavor_banner_widget.dart';

class SnowMaterialApp extends StatelessWidget {
  final Widget? home;
  final TransitionBuilder? builder;
  final RouterConfig<Object>? routerConfig;
  final String title;
  final GenerateAppTitle? onGenerateTitle;
  final ThemeData? theme;
  final ThemeData? darkTheme;
  final ThemeMode? themeMode;
  final Color? color;
  final Locale? locale;
  final Iterable<LocalizationsDelegate<dynamic>>? localizationsDelegates;
  final LocaleListResolutionCallback? localeListResolutionCallback;
  final LocaleResolutionCallback? localeResolutionCallback;
  final Iterable<Locale> supportedLocales;
  final bool showPerformanceOverlay;
  final bool checkerboardRasterCacheImages;
  final bool checkerboardOffscreenLayers;
  final bool showSemanticsDebugger;
  final bool debugShowCheckedModeBanner;
  final Map<LogicalKeySet, Intent>? shortcuts;
  final bool debugShowMaterialGrid;
  final bool useResponsive;
  final bool showFlavorBanner;
  final Widget? builderResponsive;
  final GlobalKey<ScaffoldMessengerState>? scaffoldMessengerKey;

  const SnowMaterialApp({
    Key? key,
    this.scaffoldMessengerKey,
    this.routerConfig,
    this.home,
    this.builder,
    this.title = 'Flutter project sample base',
    this.onGenerateTitle,
    this.color,
    this.theme,
    this.darkTheme,
    this.themeMode,
    this.locale,
    this.localizationsDelegates,
    this.localeListResolutionCallback,
    this.localeResolutionCallback,
    this.supportedLocales = const [
      Locale('en', 'US'),
      Locale('pt', 'BR'),
    ],
    this.debugShowMaterialGrid = false,
    this.showPerformanceOverlay = false,
    this.checkerboardRasterCacheImages = false,
    this.checkerboardOffscreenLayers = false,
    this.showSemanticsDebugger = false,
    this.debugShowCheckedModeBanner = true,
    this.shortcuts,
    this.useResponsive = true,
    this.showFlavorBanner = true,
    this.builderResponsive,
  }) : super(key: key);

  @override
  MaterialApp build(BuildContext context) {
    final mediaQuery = MediaQuery.maybeOf(context);

    return MaterialApp.router(
      title: title,
      scaffoldMessengerKey: scaffoldMessengerKey,
      locale: locale,
      builder: (context, child) {
        child ??= const SizedBox.shrink();
        return _AppBuilder(
          useResponsive: useResponsive,
          showFlavorBanner: showFlavorBanner,
          builderResponsive: builderResponsive,
          child: builder != null ? builder!(context, child) : child,
        );
      },
      supportedLocales: supportedLocales,
      localizationsDelegates: localizationsDelegates ??
          [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
      theme: theme ??
          ThemeData(
            brightness: Brightness.light,
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: {
                TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
                TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
              },
            ),
            cupertinoOverrideTheme: CupertinoThemeData(
              brightness: Brightness.light,
            ),
          ),
      darkTheme: darkTheme ??
          ThemeData(
            brightness: Brightness.dark,
            textTheme: TextTheme().apply(
              bodyColor: Colors.white,
              displayColor: Colors.white,
              decorationColor: Colors.white,
            ),
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: {
                TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
                TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
              },
            ),
            cupertinoOverrideTheme: CupertinoThemeData(
              brightness: Brightness.dark,
            ),
          ),
      themeMode: themeMode ??
          (mediaQuery?.platformBrightness == null
              ? ThemeMode.system
              : mediaQuery?.platformBrightness == Brightness.dark
                  ? ThemeMode.dark
                  : ThemeMode.light),
      onGenerateTitle: onGenerateTitle,
      color: color,
      localeListResolutionCallback: localeListResolutionCallback,
      localeResolutionCallback: localeResolutionCallback,
      showPerformanceOverlay: showPerformanceOverlay,
      checkerboardRasterCacheImages: checkerboardRasterCacheImages,
      checkerboardOffscreenLayers: checkerboardOffscreenLayers,
      showSemanticsDebugger: showSemanticsDebugger,
      debugShowCheckedModeBanner: debugShowCheckedModeBanner,
      shortcuts: shortcuts,
      debugShowMaterialGrid: debugShowMaterialGrid,
      routerConfig: routerConfig,
    );
  }
}

class _AppBuilder extends StatelessWidget {
  final Widget child;
  final Widget? builderResponsive;
  final bool useResponsive;
  final bool showFlavorBanner;

  const _AppBuilder({
    Key? key,
    required this.child,
    this.useResponsive = false,
    this.showFlavorBanner = true,
    this.builderResponsive,
  }) : super(key: key);

  Widget get body => responsiveWrapper(
        flavorBanner(child),
      );

  Widget responsiveWrapper(Widget child) => useResponsive
      ? builderResponsive ??
          ResponsiveBreakpoints.builder(
            child: child,
            breakpoints: [
              const Breakpoint(start: 0, end: 450, name: MOBILE),
              const Breakpoint(start: 451, end: 800, name: TABLET),
              const Breakpoint(start: 801, end: 1920, name: DESKTOP),
              const Breakpoint(start: 1921, end: double.infinity, name: '4K'),
            ],
          )
      : child;

  Widget flavorBanner(Widget child) =>
      showFlavorBanner ? FlavorBannerWidget(child: child) : child;

  @override
  Widget build(BuildContext context) {
    ThemeDataSnow.setIsDark(context);
    return body;
  }
}
