import 'package:catcher_2/model/localization_options.dart';
import 'package:catcher_2/model/platform_type.dart';
import 'package:catcher_2/model/report.dart';
import 'package:catcher_2/model/report_handler.dart';
import 'package:flutter/cupertino.dart';

class ErrorReporterHandler extends ReportHandler {
  final Future<void> Function(FlutterErrorDetails errorDetails) errorReporter;
  final bool _shouldHandleWhenRejected;
  ErrorReporterHandler(this.errorReporter,
      {LocalizationOptions? localizationOption, bool? shouldHandleWhenRejected})
      : _localizationOptions = localizationOption,
        _shouldHandleWhenRejected = shouldHandleWhenRejected ?? false;

  @override
  List<PlatformType> getSupportedPlatforms() {
    return [
      PlatformType.android,
      PlatformType.iOS,
      PlatformType.web,
      PlatformType.unknown,
    ];
  }

  ///Location settings
  LocalizationOptions? _localizationOptions;

  @override
  LocalizationOptions get localizationOptions =>
      _localizationOptions ?? LocalizationOptions.buildDefaultEnglishOptions();

  @override
  void setLocalizationOptions(LocalizationOptions? localizationOptions) {
    _localizationOptions = localizationOptions;
  }

  @override
  Future<bool> handle(Report error, BuildContext? context) async {
    try {
      final errorDetails = error.errorDetails;
      if (errorDetails != null) {
        await errorReporter(errorDetails);
      }
      return true;
      // ignore: empty_catches
    } catch (e) {}
    return false;
  }

  @override
  bool isContextRequired() {
    return false;
  }

  @override
  bool shouldHandleWhenRejected() {
    return _shouldHandleWhenRejected;
  }
}
