import 'package:dio/dio.dart';

class LogInterceptor extends InterceptorsWrapper {
  LogInterceptor()
      : super(
          onRequest: (options, handler) {
            print(
                'METHOD: ${options.method} \nDATA: [${options.data}] \nURL: ${options.uri}');
            handler.next(options);
          },
          onResponse: (response, handler) {
            print(
                'RESPONSE: ${response.statusCode} \n Data: ${response.data} \n Data Length: ${response.data.length}');
            handler.next(response);
          },
          onError: (error, handler) {
            print(
                'ERROR: ${error.response?.statusCode} \n Message: ${error.message} \n Data: ${error.response?.data}');
            handler.next(error);
          },
        );
}
