# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.3.0] - 2021-06-04

- Moved `Resource`, `ResourceWidgets` and `NetworkBoundResources` to [resource_network_fetcher](https://pub.dev/packages/resource_network_fetcher).
Because by this way we provide a better decision to the developer of what he wants to use and import. Another thing that
was improved is the documentation about this part of the package.

## [2.2.4] - 2021-04-30

- Updated catcher version

## [2.2.3] - 2021-04-30

## Improvements

- Improved loading widgets of orderable lists with `ListViewResource`.

## [2.2.0] - 2021-04-01

## Breaking changes

- Removed the `NetworkBoundResources()` constructor and transformed all methods to static ( you can follow the
Readme.md to help you with the migration.

## Improvements

- Added `metaData` property to the `Resource` object that will be useful with stream methods. It will store the last
movements of the results of the stream (last 2 results, for better performance).
- All `stream` methods of `NetworkBoundResources` were adapted to use the `metaData` of `Resource`.

## [2.1.1] - 2021-03-23

## Fixes

- Fixed LogInterceptor

## [2.1.0] - 2021-03-17

## Breaking changes

- Changed dependency `reordable` to `flutter_reorderable_list`, because the other is discontinued.

## [2.0.0-nullsafety.1] - 2021-03-17

## Improvements

- Updated dependencies (catcher, dio, flutter_modular and responsive_framework)

## [2.0.0-nullsafety.0] - 2021-03-10

## Improvements

- All the package is now in null-safety!! (some packages is in pre-release for null safety, then because of this, this
  package is not on 2.0.0)

### Breaking changes

- Removed the dependency and feature of `device_preview`

## [1.2.0-nullsafety.0] - 2021-03-03

### Breaking changes

- Removed `CacheInterceptor`, because was causing too much complications when the project do not use this interceptor,
  this happens because whe used a package called `dio_http_cache`, and the updates to null-safety was breaking projects
  and this package.


## [1.1.0-nullsafety.0] - 2021-01-29

## Improvements

- Improved reordable list with the package `reordables` (the design is suddenly different between the until version
  that used the native `ReordebleListView`)

## [1.0.0-nullsafety.2] - 2020-12-15

### Breaking changes

- Removed `ListViewWidget`
- Updated all packages to support null safety
- Updated `flutter_modular` to `3.0.0`

### Improvements

- Updated all code to support null safety

### Fixes

- Fixed issue [#18](https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/issues/18)

## [0.2.2] - 2020-12-08

### Fixes

- Fixed issue [#17](https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/issues/17)

## [0.2.1] - 2020-11-27

### Added

- Added `reorderable` option to `ListViewResourceWidget`

## [0.2.0] - 2020-11-18 (Breaking changes)

### Breaking changes

- Changed the name `ListViewWidget` to `ListViewResourceWidget`

### Added

- Added variables of `ListView` in `ListViewResourceWidget`
- Added method `asSimpleStream` in `NetworkBoundResources` that allows use a simple stream and transforms
  only the result.
- Added method `asResourceStream` in `NetworkBoundResources` that allows use a simple stream and transforms
  only the result, with the differential that the stream can send by result a `Resource`, so leaving the responsible
  to stream to control if is on error or not.
- Removed the params `shouldFetch` and `saveCallResult` of required to optional of method `asStream` in `NetworkBoundResources`
- Added automatic stream close when using loadFromDbFuture
- Added `asRequest` method in `Resource`, that map errors on functions that does not returns a `Future`.

### Fixes

- Fixed pedantic issues
- Fixed add sink events on closed sinks

## [0.1.7] - 2020-11-17

### Added

- Added `errorWithDataWidget` and `loadingWithDataWidget` attributes on `ResourceWidget`

## [0.1.6] - 2020-11-16

### Fixes

- Export Widget `ErrorSnowWidget`
- Removed unused padding attribute of `ResourceWidget`
- Fixed `errorWidget` of `ResourceWidget`

## [0.1.5] - 2020-10-27

### Fixes

- Export class `UIHelper`

## [0.1.4] - 2020-10-26

### Added

- `WidgetUtil`
- `ColorUtil`
- `CurrencyPtBrInputFormatter` to pt-br currency mask
- `UIHelper` to horizontal and vertical spacement
- Add error log to `Resource.asFuture` when failed

## [0.1.3] - 2020-09-10

## Added

- Added `ListViewWidget`, that helps you to create a list of widgets with a resource, that have 5 states, error,
error with data, loading, loading with data and success. With `RefreshIndicator` and a default `ErrorWidget`.
- Added `ResourceWidget`, that helps you to create a widget with a resource, that have 3 states, error, loading
and success. With a default `ErrorWidget` with a callback function.

### Fixes

- Updated packages

## [0.1.2] - 2020-08-31

### Added

- Updated version of `flutter_modular`

## [0.1.1] - 2020-07-29

### Added

- Added `transformData` and `mergeStatus` methods in Resource object.
- Improvement when occur an error with `Resource.asFuture()`.
- Bug fixes.

## [0.1.0] - 2020-06-26

### Added

- Initial version of the package.
- Thanks to [Denis Viana](https://gitlab.com/denisviana) and to [Lucas Polazzo](https://gitlab.com/LucazzP) that made
  this first version of the package!

[Unreleased]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/compare/master...v2.2.0
[2.2.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v2.2.0
[2.1.1]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v2.1.1
[2.1.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v2.1.0
[2.0.0-nullsafety.1]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v2.0.0-nullsafety.1
[2.0.0-nullsafety.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v2.0.0-nullsafety.0
[1.2.0-nullsafety.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v1.2.0-nullsafety.0
[1.1.0-nullsafety.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v1.1.0-nullsafety.0
[1.0.0-nullsafety.2]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v1.0.0-nullsafety.2
[0.2.2]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.2.2
[0.2.1]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.2.1
[0.2.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.2.0
[0.1.7]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.7
[0.1.6]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.6
[0.1.5]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.5
[0.1.4]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.4
[0.1.3]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.3
[0.1.2]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.2
[0.1.1]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.1
[0.1.0]: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow/-/tags/v0.1.0
